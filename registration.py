# $Id$
#
# SIP account and registration sample. In this sample, the program
# will block to wait until registration is complete
#
# Copyright (C) 2003-2008 Benny Prijono <benny@prijono.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
#
import sys
import pjsua as pj
import threading


class ParseArguments():

    def parse(self, args=sys.argv[1:]):
        parser = argparse.ArgumentParser()

        # for multiple chooses add action="append" will fill list structure, accessbile as array by index
        # for requared fields add required=True
        # fields withouany value add action="store_true"

        parser.add_argument("--id")
        parser.add_argument("--registrar")
        parser.add_argument("--realm")
        parser.add_argument("--username")
        parser.add_argument("--password")
        parser.add_argument("--contact")
        parser.add_argument("--outbound")
        parser.add_argument("--thread-cnt")
        parser.add_argument("--nameserver", action="append")
        parser.add_argument("--publish", action="store_true")
        parser.add_argument("--clock-rate")
        parser.add_argument("--add-codec")
        parser.add_argument("--dis-codec", action="append")

        options = parser.parse_args(args)
        return options


def log_cb(level, str, len):
    print str,

class MyAccountCallback(pj.AccountCallback):
    sem = None

    def __init__(self, account):
        pj.AccountCallback.__init__(self, account)

    def wait(self):
        self.sem = threading.Semaphore(0)
        self.sem.acquire()

    def on_reg_state(self):
        if self.sem:
            if self.account.info().reg_status >= 200:
                self.sem.release()

# create object for parser and parse list of arguments
parser = ParseArguments()
args = parser.parse()

# now you can call any value by args.id or any variable listed above without dashes

lib = pj.Lib()

try:
    lib.init(log_cfg = pj.LogConfig(level=4, callback=log_cb))
    lib.create_transport(pj.TransportType.UDP, pj.TransportConfig(5080))
    lib.start()

    acc = lib.create_account(pj.AccountConfig("pjsip.org", "bennylp", "***"))

    acc_cb = MyAccountCallback(acc)
    acc.set_callback(acc_cb)
    acc_cb.wait()

    print "\n"
    print "Registration complete, status=", acc.info().reg_status, \
          "(" + acc.info().reg_reason + ")"
    print "\nPress ENTER to quit"
    sys.stdin.readline()

    lib.destroy()
    lib = None

except pj.Error, e:
    print "Exception: " + str(e)
    lib.destroy()

